// Required here even if only used in `tests` modules
#[cfg(test)]
#[macro_use]
extern crate approx;

mod core;

fn main() {
    let v = core::Vector {
        x: 1.0,
        y: 2.0,
        z: 2.0,
    };

    println!("Hello, world!");
}
