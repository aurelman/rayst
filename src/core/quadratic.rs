/**
 * The possible roots a quadratic equation can have
 */
#[derive(Debug, PartialEq)]
pub enum Root {
    /// When there are no roots
    None,
    /// When there is only one root
    One(f64),
    /// When there are two roots (in that case the first one will have the lowest of both values)
    Two(f64, f64),
}

/**
 * Represents an equation of the form `ax² + bx + c`
 */
#[derive(Debug)]
pub struct QuadraticEquation {
    pub a: f64,
    pub b: f64,
    pub c: f64,
}

impl QuadraticEquation {

    /**
     * Solves the current equation and return its possible roots if any.
     */
    pub fn solve(&self) -> Root {
        if self.a == 0. {
            return Root::One(-self.b / self.c);
        }

        let determinant = self.b * self.b - 4. * self.a * self.c;

        if determinant < 0. {
            return Root::None;
        }

        if determinant == 0. {
            return Root::One(-self.b / 2. * self.a);
        }

        let root_squared_delta = determinant.sqrt();

        /*
         *  The numerically computed solution would be  :
         *   double root1 = (-b - rootSquaredDelta) / 2a;
         *   double root2 = (-b + rootSquaredDelta) / 2a;
         *
         *  But to avoid cancellation issues in case b*b is much greater that 4*a*c
         *  we compute roots in an other way.
         *
         *  For more details about cancellation see : https://en.wikipedia.org/wiki/Loss_of_significance
         */
        let first = (-self.b - self.b.signum() * root_squared_delta) / (2. * self.a);
        let second = self.c / (self.a * first);

        if first < second {
            return Root::Two(first, second);
        }

        return Root::Two(second, first);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_solves_with_no_roots() {
        let e = QuadraticEquation {
            a: 2.,
            b: -1.,
            c: 2.,
        };

        assert_eq!(e.solve(), Root::None)
    }

    #[test]
    fn it_solves_with_one_root() {
        let e = QuadraticEquation {
            a: 0.,
            b: 5.,
            c: 2.,
        };

        if let Root::One(root) = e.solve() {
            assert_relative_eq!(root, -5.0 / 2., epsilon = f64::EPSILON)
        } else {
            panic!("test failure");
        }
    }

    #[test]
    fn it_solves_with_one_root_with_a_different_from_0() {
        let e = QuadraticEquation {
            a: 1.,
            b: 2.,
            c: 1.,
        };

        if let Root::One(root) = e.solve() {
            assert_relative_eq!(root, -1., epsilon = f64::EPSILON);
        } else {
            panic!("test failure");
        }
    }

    #[test]
    fn it_solves_with_two_roots() {
        let e = QuadraticEquation {
            a: 2.,
            b: 5.,
            c: 2.,
        };

        if let Root::Two(first, second) = e.solve() {
            assert_relative_eq!(first, -2., epsilon = f64::EPSILON);
            assert_relative_eq!(second, -0.5, epsilon = f64::EPSILON);
        } else {
            panic!("test failure");
        }
    }

    #[test]
    fn it_avoids_cancellation_issues() {
        // e with possible cancellation issues
        let e = QuadraticEquation {
            a: 1.,
            b: 200.,
            c: -0.000015,
        };

        if let Root::Two(first, second) = e.solve() {
            assert_relative_eq!(first, -200.000000075, epsilon = f64::EPSILON);
            assert_relative_eq!(second, 0.000000075, epsilon = f64::EPSILON);
        } else {
            panic!("test failure");
        }
    }
}
