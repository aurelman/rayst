use std::ops::{Add, Mul, Neg, Sub};

#[derive(Debug, PartialEq)]
pub struct Vector {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Vector {
    pub fn build(x: f64, y: f64, z: f64) -> Vector {
        Vector { x, y, z }
    }

    pub fn squared_length(&self) -> f64 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    pub fn length(&self) -> f64 {
        self.squared_length().sqrt()
    }
}

impl Neg for Vector {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl Add for Vector {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z,
        }
    }
}

impl Sub for Vector {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl Mul<f64> for Vector {
    type Output = Self;

    fn mul(self, rhs: f64) -> Self::Output {
        Self {
            x: self.x * rhs,
            y: self.y * rhs,
            z: self.z * rhs,
        }
    }
}

// Implemented multiplication of a float by a Vector (make the multiplication commutative)
impl Mul<Vector> for f64 {
    type Output = Vector;

    fn mul(self, rhs: Vector) -> Self::Output {
        rhs * self
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_build_vector_object() {
        let v = Vector::build(1., 2., 3.);

        assert_eq!(v.x, 1.0);
        assert_eq!(v.y, 2.0);
        assert_eq!(v.z, 3.0);
    }

    #[test]
    fn it_computes_squared_length() {
        let v = Vector::build(1., 5., -12.);

        assert_relative_eq!(v.squared_length(), 170., epsilon = f64::EPSILON);
    }

    #[test]
    fn it_computes_length() {
        let v = Vector::build(1., 5., -12.);

        assert_relative_eq!(v.length(), 13.038404810405298, epsilon = f64::EPSILON);
    }

    #[test]
    fn it_compares_vectors() {
        let v1 = Vector::build(2., 5., -8.);
        let v2 = Vector::build(2., 5., -8.);

        assert_eq!(v1, v2);
    }

    #[test]
    fn it_adds_vectors() {
        let v1 = Vector::build(1., 5., -8.);
        let v2 = Vector::build(2., -3., 4.);

        let expected = Vector::build(3., 2., -4.);

        assert_eq!(v1 + v2, expected);
    }

    #[test]
    fn it_subtracts_vectors() {
        let v1 = Vector::build(1., 5., -8.);
        let v2 = Vector::build(2., -3., 4.);

        let expected = Vector::build(-1., 8., -12.);

        assert_eq!(v1 - v2, expected);
    }

    #[test]
    fn it_multiplies_vector_by_a_scalar() {
        let v = Vector::build(1., 5., -8.);

        let expected = Vector::build(2., 10., -16.);

        assert_eq!(v * 2., expected);
    }

    #[test]
    fn it_multiplies_vector_a_scalar_by_a_vector() {
        let v = Vector::build(1., 5., -8.);

        let expected = Vector::build(2., 10., -16.);

        assert_eq!(2. * v, expected);
    }

    #[test]
    fn it_negates_a_vector() {
        let v = Vector::build(1., 5., -8.);

        let expected = Vector::build(-1., -5., 8.);

        assert_eq!(-v, expected);
    }
}
